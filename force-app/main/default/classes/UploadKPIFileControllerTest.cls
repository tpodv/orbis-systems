@isTest
public class UploadKPIFileControllerTest {

  @testSetup static void setup() {
    // Create Profit Centers
    string[] profitCenters =new string[] {'11214','31214','41214','11218','11250','11230','41230','11260','11270'};
    List<Profit_Centre__c> pcList = new List<Profit_Centre__c>();
    for( String pcCode :  profitCenters) {
      Profit_Centre__c pc = OrbisTestDataFactory.getProfitCenter(pcCode);
      pcList.add(pc);
    }
    insert pcList;
  }

  private static testMethod void uploadKPIFileTest(){
    StaticResource srObject = [SELECT Id, Body FROM StaticResource WHERE Name = 'ForecastingKPIFile'];
    String fileContent = srObject.body.toString();
    integer currentYear = system.today().year();

    UploadKPIFileController.createKPIOpportunities(fileContent);
    Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Won Opportunity').getRecordTypeId();
    List<Opportunity> optyLst = [SELECT Id, StageName FROM Opportunity WHERE RecordTypeId =:recordTypeId];
    //system.assertEquals(16, optyLst.size());
    //system.assertEquals('Closed Won', optyLst[0].StageName);
    Date closeDate = date.newInstance(currentYear, 4, 1);
    List<Opportunity> optyLstApril = [SELECT Id, StageName FROM Opportunity WHERE RecordTypeId =:recordTypeId AND CloseDate=:closeDate];
    //system.assertEquals(2, optyLstApril.size());
    UploadKPIFileController.createKPIOpportunities(fileContent);
    string optyName = 'Forecast_'+currentYear+'_4_11214';
    List<Opportunity> optyNamed = [SELECT Id, StageName FROM Opportunity WHERE Name=:optyName];
    //system.assertEquals(1, optyNamed.size());
    fileContent =';;;;;;;;;;/n;;;;;;;';
    UploadKPIFileController.createKPIOpportunities(fileContent);
  }


}