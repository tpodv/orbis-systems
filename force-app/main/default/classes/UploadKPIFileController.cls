public class UploadKPIFileController {

  @AuraEnabled
  public static string createKPIOpportunities(string fileContent) {
    String result;
    String[] filelines = new String[] {};
    String stringFile = EncodingUtil.urlDecode(fileContent, 'UTF-8');
    stringFile = stringFile.replace(';', ',');
    system.debug('stringFile'+stringFile);
    String profitCentre;
    Map<Integer, Double> monthForecastMap = new Map<Integer, Double>();
    Map <String, List<Map<Integer,Double> > > profitCentreForecastMap = new Map <String, List<Map<Integer,Double> > >();
    Map<Integer,Integer> columnMonthMap = new Map<Integer,Integer>();

    try{
      filelines = stringFile.split('\n');
      string year = getKPIyear(filelines[1]);
      if( year == null ) {
        return 'KPI mapping year is not found';
      }
      columnMonthMap = getColumnsMonths(filelines[2], year);
      for ( integer i=3; i < filelines.size(); i++ ) {
        String[] inputvalues = new String[] {};
        inputvalues = filelines[i].split(',',-1);
        if( inputvalues[0] != '' && inputvalues[0] != 'Invoicing' && profitCentre!=null)
          i = filelines.size();
        else if( inputvalues[1].isNumeric()) {
          profitCentre = string.valueOf(inputvalues[1]);
        }else if( profitCentre != null && inputvalues[2] != null) {
          system.debug('File line: '+filelines[i]);
          monthForecastMap = getMonthForecast(filelines[i], columnMonthMap);
          profitCentreForecastMap = getProfitCentreForecasts(profitCentre, monthForecastMap, profitCentreForecastMap);
        }
      }
      result = createOpportunities(year, profitCentreForecastMap);
    }
    catch(Exception e) {
      system.debug('Error message: '+e.getMessage());
      result = 'KPI file failed to load. '+e.getMessage();
    }

    system.debug('Result string='+result);

    return result;
  }

  private static string createOpportunities(string year, Map <String, List<Map<Integer,Double> > > profitCentreForecastMap){
    string result;
    if( year == null || year == '' )
      result = 'KPI mapping year is not found';
    else if( profitCentreForecastMap == null ) {
      result = 'No Forecast values provided';
    }else{
      try{
        List<Opportunity> optyLst = new List<Opportunity>();
        Map<String,Id> profitCentreIdMap = getProfitCentersIdMap(profitCentreForecastMap);
        for( String profitCentre : profitCentreForecastMap.keySet()) {
          List<Map<Integer,Double> > monthForecastMapList = profitCentreForecastMap.get(profitCentre);
          optyLst.addAll(getOpportunities(year, monthForecastMapList, profitCentreIdMap, profitCentre));
        }
        if( !optyLst.isEmpty() ) {
          deleteExistingOpportunities(year);
          insert optyLst;
          result= optyLst.size()+' Opportunity record(s) were created sucessfully';
        }
        else
          result ='No Opportunities to insert';
      }
      catch(Exception e) {
        result = 'KPI file upload is failed. '+e.getMessage();
      }
    }
    return result;
  }

  private static List<Opportunity> getOpportunities(string year, List<Map<Integer,Double> > monthForecastMapList,
                                                    Map<String,Id> profitCentreIdMap, string profitCentre){
    List<Opportunity> optyLst = new List<Opportunity>();
    Id accountId = getAccountId();
    for(Map<Integer,Double> monthForecastMap : monthForecastMapList) {
      for( Integer month : monthForecastMap.keySet() ) {
        double amount = monthForecastMap.get(month);
        if( amount != null && amount > 0 ) {
          Opportunity opty = getForecastOpportunity(year, month, accountId, amount,
                                                      profitCentreIdMap.get(profitCentre));
          if( opty != null ) {
            opty.Name = opty.Name + profitCentre;
            optyLst.add(opty);
          }
        }
      }
    }
    return optyLst;
  }

  private static Id getAccountId(){
    Id accountId;
    string accountName = 'Orbis Internal Forecasting';
    List<Account> accLst = [SELECT Id FROM Account WHERE Name =:accountName];
    if( !accLst.isEmpty())
      accountId = accLst[0].Id;
    else{
      Account newAcc = OrbisTestDataFactory.getAccount(accountName,null);
      insert newAcc;
      accountId = newAcc.Id;
    }
    return accountId;
  }

  public static void deleteExistingOpportunities(string year){
    Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Won Opportunity').getRecordTypeId();

    List<Opportunity> optyLst = [SELECT Id
                                 FROM Opportunity
                                 WHERE RecordTypeId=:recordTypeId];
    if( !optyLst.isEmpty())
      delete optyLst;
  }

  private static Map<String,Id> getProfitCentersIdMap(Map <String, List<Map<Integer,Double> > > forecastMap){
    Map<String,Id> profitCentreIdMap = new Map<String,Id>();
    Set<String> profitCentreNames = new Set<String>();
    for( String profitCentre : forecastMap.keyset() )
      profitCentreNames.add(profitCentre);
    List<Profit_Centre__c> profitCenterLst = [SELECT Id, Profit_Centre_Code__c FROM Profit_Centre__c
                                              WHERE Profit_Centre_Code__c IN : profitCentreNames];
    for( Profit_Centre__c pc :  profitCenterLst)
      profitCentreIdMap.put(pc.Profit_Centre_Code__c, pc.Id);
    return profitCentreIdMap;
  }

  private static Opportunity getForecastOpportunity(string year, integer month, id accountId,
                                                    double forecastAmount, id profitCentreId){
    Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Won Opportunity').getRecordTypeId();
    Opportunity opty = new Opportunity();
    opty.AccountId = accountId;
    opty.RecordTypeId = recordTypeId;
    opty.Profit_centre__c = profitCentreId;
    opty.Amount = forecastAmount;
    opty.StageName ='Closed Won';
    opty.CloseDate = date.newInstance(integer.valueOf(year), month, 1);
    opty.LeadTimeWeeks__c = 1;
    opty.PO_received__c = true;
    opty.Handover_to_operations_done__c = true;
    opty.Name = 'Won_'+year+'_'+month+'_';
    return opty;
  }

  private static string getKPIyear(string headerline){
    string year;
    string header;
    String[] inputvalues = new String[] {};
    inputvalues = headerline.split(',');
    header = inputvalues[0];
    if(header.containsIgnoreCase('KPI mapping chart'))
      year = header.right(4);
    return year;
  }

  private static Map <String, List<Map<Integer,Double> > > getProfitCentreForecasts(string profitCentre,
                                                                                    Map<Integer, Double> monthForecastMap,
                                                                                    Map <String, List<Map<Integer,Double> > > pcForecastMap){
    List<Map<Integer,Double> > pcForecastMapList = new List<Map<Integer,Double> >();
    if( pcForecastMap.containsKey(profitCentre) )   {
      // Get the list of aggregated month forecasts for given profit centre
      List<Map<Integer,Double> > allMonthForecastMap = pcForecastMap.get(profitCentre);
      for( Map<Integer,Double> monthlyForecasts : allMonthForecastMap ) {
        for( Integer month : monthlyForecasts.keySet() ) {
          double cellValue = monthForecastMap.get(month);
          Double monthForecast = cellValue != null  ? cellValue : 0;
          monthlyForecasts.put(month, monthlyForecasts.get(month) + monthForecast);
        }
        pcForecastMapList.add(monthlyForecasts);
        system.debug('monthly forecast='+monthlyForecasts);
      }
    } else {
      pcForecastMapList.add(monthForecastMap);
    }
    pcForecastMap.put(profitCentre, pcForecastMapList);

    return pcForecastMap;
  }

  private static Map<Integer,Double> getMonthForecast(string filestring, Map<Integer,Integer> columnsMonthsMap){
    Map<Integer,Double> monthForecastMap = new Map<Integer,Double>();
    String[] inputvalues = filestring.split(',',-1);
    system.debug('filestring for getMonthForecast='+filestring);
    system.debug(inputvalues.size()+' inputvalues found='+inputvalues);
    for( Integer month : columnsMonthsMap.keySet()) {
      integer valueIndex = columnsMonthsMap.get(month);
      string cellValue = inputvalues[valueIndex];
      cellValue = OrbisUtils.getClearlearString(cellValue);
      system.debug('forecast value for month '+month+'='+cellValue);
      Double forecastValue = cellValue != null && cellValue != '' ? Double.valueof(cellValue) : 0;
      monthForecastMap.put(month, forecastValue);
    }
    return monthForecastMap;
  }

  private static Map<Integer,Integer> getColumnsMonths( string inputstring, string year ){
    Map<Integer,Integer> columnMonthMap = new Map<Integer,Integer>();
    String[] inputvalues = inputstring.split(',',-1);
    string value;
    integer month = 0;
    string[] forecastHeader;
    integer currentPeriodStamp = getPeriodStamp(Date.today().month(), Date.today().year());
    integer opportunityPeriodStamp;
    system.debug('current period stamp='+currentPeriodStamp);
    for( integer i = 0; i < inputvalues.size(); i++ ) {
      value = inputvalues[i];
      if( value.containsIgnoreCase('forecast')) {
        month = getMonth(value,'/');
        opportunityPeriodStamp = getPeriodStamp(month, integer.valueOf(year));
        if( opportunityPeriodStamp >= currentPeriodStamp )
          columnMonthMap.put(month,i);
      }else if( value.containsIgnoreCase('actual')) {
        String spaceChar =  OrbisUtils.getAsciiCode(32);
        month = getMonth(value,spaceChar);
        opportunityPeriodStamp = getPeriodStamp(month, integer.valueOf(year));
        if( opportunityPeriodStamp < currentPeriodStamp )
          columnMonthMap.put(month,i);
      }
    }
    system.debug('columnMonthMap='+ columnMonthMap);
    return columnMonthMap;
  }

  private static integer getPeriodStamp(integer month, integer year){
    string currentMonth = String.valueOf(month);
    string currentYear = String.valueOf(year);
    currentMonth = currentMonth.length() < 2 ? '0'+currentMonth : currentMonth;
    string dateStr = currentYear + currentMonth;
    integer currentPeriod = Integer.valueOf(dateStr);
    return currentPeriod;
  }

  private static integer getMonth(string value, string separator){
    integer month = 0;
    string[] periodValue = value.split(separator);
    month = integer.valueOf(periodValue[0].substring(1).trim());
    return month;
  }

}