@isTest
public class OpportunityOutdatedNotificationTest {
  @testSetup static void setup() {
    List <Account> accLst = new List<Account>();
    integer i;
    Profit_Centre__c pc = OrbisTestDataFactory.getProfitCenter('11214');
    insert pc;
    for( i = 0; i<110; i++) {
      Account acc = OrbisTestDataFactory.getAccount('Account_'+1, pc.Id);
      accLst.add(acc);
    }
    insert accLst;
    List<Opportunity> optyLst = new List<Opportunity>();
    List<User> userLst = new List<User>();
    userLst.add(OrbisTestDataFactory.getUser('TestUser1'));
    userLst.add(OrbisTestDataFactory.getUser('TestUser2'));
    insert userLst;
    for( Account acc : accLst ) {
      date closeDateExpired = system.today().addDays(-5);
      date closeDateOpen = system.today().addDays(5);
      Opportunity optyOutdated = OrbisTestDataFactory.getOpportunity('Identify Opportunity Closed'+acc.Name, 'Identify Opportunity', acc.Id, closeDateExpired);
      Opportunity opty = OrbisTestDataFactory.getOpportunity('Identify Opportunity Closed'+acc.Name,'Identify Opportunity', acc.Id, closeDateOpen);
      Opportunity optyClosed = OrbisTestDataFactory.getOpportunity('Closed Won'+acc.Name, 'Closed Won', acc.Id, closeDateOpen);
      optyLst.add(opty);
      optyLst.add(optyOutdated);
      optyLst.add(optyClosed);
    }
    insert optyLst;
  }

  static testMethod void runOpportunityNotificationBatch(){
    Test.startTest();
    Integer emailsSentBefore    = Limits.getEmailInvocations();
    OpportunityOutdatedNotificationBatch notificationBatch = new OpportunityOutdatedNotificationBatch();
    String sch = '0 0 23 * * ?';
    system.schedule('Opportunity Outdated Notification', sch, notificationBatch);
    Integer emailsSentAfter   = Limits.getEmailInvocations();
    Test.stopTest();
    system.debug('emailsSentBefore='+emailsSentBefore +' emailsSentAfter='+emailsSentAfter);
  }
}