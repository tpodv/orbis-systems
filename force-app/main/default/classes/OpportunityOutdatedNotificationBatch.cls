global class OpportunityOutdatedNotificationBatch implements Schedulable {

  private static date todayDate = date.today();

  public void execute(SchedulableContext sc){
    system.debug('execute started');
    Map<String, List<Opportunity> > optyOwnerMap = new Map<String, List<Opportunity> >();
    string optyLink;
    List<Messaging.SingleEmailMessage> emailList;
    string subj = 'You have expired Salesforce Opportunities';
    String soql = 'SELECT Id, CloseDate, Name, OwnerId, Owner.Email, Owner.FirstName, Owner.LastName '+
                  'FROM Opportunity WHERE CloseDate<=:todayDate AND IsClosed = FALSE'+
                  ' AND Outdated_opportunity_notification_flag__c = TRUE'+
                  (Test.isRunningTest() ? ' LIMIT 50' : '');
    List<Opportunity> optyLst = Database.Query(soql);
    system.debug('Opties found ='+optyLst);
    for( Opportunity opty : optyLst ) {
      if( optyOwnerMap.ContainsKey(opty.Owner.Email))
        optyOwnerMap.get(opty.Owner.Email).add(opty);
      else
        optyOwnerMap.put(opty.Owner.Email, new List<Opportunity> {opty});
    }

    emailList = new List<Messaging.SingleEmailMessage>();
    for ( String owner :  optyOwnerMap.keySet() ) {
      List<Opportunity> opportunities = optyOwnerMap.get(owner);
      string expiredOpties;
      for( Opportunity opty : opportunities ) {
        optyLink = '<br/><a href="'+URL.getSalesforceBaseUrl().toExternalForm() + '/'+opty.Id +
                   '" target="_blank">'+opty.Name +
                   ' - PO Estimated Date:'+opty.CloseDate+'</a><br/>';
        expiredOpties = expiredOpties == null ? optyLink : expiredOpties + optyLink;
      }
      system.debug('Expired opties for user '+owner+' : '+expiredOpties);
      string emailBody = 'The following opportunities have been expired: <br/>'+expiredOpties+
                         '<br/><br/><br/> In case you don\'t want to receive notification for certain opportunity, you can uncheck Outdated Opportunity Notification checkbox on Opportunity record';
      Messaging.SingleEmailMessage email = OrbisUtils.getEmail(emailBody, subj, new List<String> {owner});
      emailList.add(email);
    }
    Messaging.sendEmail(emailList);
  }
}