public class OrbisTestDataFactory {

  public static User getUser(string name){
    Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
    User u = new User(Alias = 'test1',
                      Email=name+name+'.test@gamil.com',
                      FirstName = 'Test',
                      EmailEncodingKey='UTF-8',
                      LastName=name,
                      LanguageLocaleKey='en_US',
                      LocaleSidKey='en_US',
                      ProfileId = p.Id,
                      TimeZoneSidKey='America/Los_Angeles',
                      UserName=name+'@fjhdabdjbfsakdfjk.fi');
    return u;
  }

  public static Account getAccount(string name, string profitCenterId){
    Account acc = new Account();
    acc.Profit_Centre__c = profitCenterId;
    acc.name = name;
    return acc;
  }

  public static Contact getContact(Id accId, string name){
    Contact con = new Contact();
    con.LastName = name;
    con.AccountId = accId;
    return con;
  }

  public static Opportunity getOpportunity(string name, string stage, id accId, date closeDate){
    Opportunity opty = new Opportunity();
    opty.Name = name;
    opty.StageName = stage;
    opty.AccountId = accId;
    opty.CloseDate = closeDate;
    opty.Amount =3000;
    opty.PO_received__c = true;
    opty.Handover_to_operations_done__c = true;
    opty.Documents_copied_to_L_drive__c = true;
    return opty;
  }

  public static Opportunity getForecastOpportunity(string name, string stage, id accId, date closeDate){
    Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Forecast Opportunity').getRecordTypeId();
    Opportunity opty = getOpportunity(name, stage, accId, closeDate);
    return opty;
  }

  public static Opportunity getSalesOpportunity(string name, string stage, id accId, date closeDate){
    Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Forecast Sales').getRecordTypeId();
    Opportunity opty = getOpportunity(name, stage, accId, closeDate);
    return opty;
  }

  public static Profit_Centre__c getProfitCenter(string code){
    Profit_Centre__c profitCenter = new Profit_Centre__c();
    profitCenter.Name = code;
    profitCenter.Profit_Centre_Code__c = code;
    return profitCenter;
  }
}