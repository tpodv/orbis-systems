@isTest(seeAllData=true)
public class OrbisBudgetingViewControllerTest {
  private static testMethod void runReportTest(){

    string snapshotJSON='';
    string snapshotRecordJSON = '","M1__c":"30000.00","CalculatedM1__c":"1700.00","M2__c":"5000.00","CalculatedM2__c":"1640.00","M3__c":"7900000.00","CalculatedM3__c":"1071386.74","M4__c":"10.00","CalculatedM4__c":"85400.00","M5__c":"0.00","CalculatedM5__c":"55000.00","M6__c":"0.00","CalculatedM6__c":"0.00","M7__c":"4560000.00","CalculatedM7__c”:”89000.00","M8__c":"0.00","CalculatedM8__c":"0.00","M9__c":"0.00","CalculatedM9__c":"0.00","M10__c":"0.00","CalculatedM10__c":"70000.00","M11__c":"0.00","CalculatedM11__c”:”7000.00","M12__c":"0.00","CalculatedM12__c":"0.00”}';
    List<Profit_Centre__c> pcList = [SELECT Id FROM Profit_Centre__c];
    for( Profit_Centre__c pc : pcList ) {
      string str = '{"Profit_centre__c":"'+pc.Id + snapshotRecordJSON;
      snapshotJSON = snapshotJSON != '' ? ','+str : str;
    }
    snapshotJSON = '['+snapshotJSON+']';
    Test.startTest();
    OrbisBudgetingViewController.getBudgetingTotals('Account', string.valueof(System.today().year()));
    OrbisBudgetingViewController.saveBudgetingInputs(snapshotJSON, 'Profit_Center');
    integer month = OrbisBudgetingViewController.getMonth('M5');
    Test.stopTest();
    System.assertEquals(5, month);
  }


}