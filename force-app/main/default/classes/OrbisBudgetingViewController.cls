public without sharing class OrbisBudgetingViewController {

// dimension values: 'Profit_Center', 'Account'
  @AuraEnabled
  public static String getBudgetingTotals(string dimension, string year){
    //Map<String,BudgetingWrapper> BudgetingMap = new Map<String,BudgetingWrapper >();
    String budgetingTotalsJSON ='{}';
    string reportName = 'Budgeting_View_' + dimension + '_Totals_wgf';
    Reports.ReportFilter reportFilter = OrbisUtils.getReportFilter('Opportunity.DeliveryYear__c', 'equals', year);
    List<Reports.ReportFilter> repFilters = new List<Reports.ReportFilter>();
    repFilters.add(reportFilter);
    Reports.ReportResults results = OrbisUtils.runReport(reportName, false, repFilters);
    if( results != null ) {
      Map<Id, Map<Integer, Decimal>> snapshotMap = getSnapshotMap(dimension, year);
      budgetingTotalsJSON = getBudgetingJSON(results, snapshotMap, year, dimension);
      system.debug(dimension + 'JSON : ' + budgetingTotalsJSON);
    }
    return budgetingTotalsJSON;
  }

  private static String getBudgetingJSON(Reports.ReportResults results, Map<Id, Map<Integer, Decimal>> snapshotMap, string year, string dimension){
    Reports.Dimension dim = results.getGroupingsDown();
    Map<String,Reports.ReportFact> factMap = (Map<String,Reports.ReportFact>)results.getFactMap();
    List<Reports.GroupingValue> groupingColumns = (List<Reports.GroupingValue>)results.getGroupingsAcross().getGroupings();
    Map<Id, Map<String, String>> opportunityTotalsMap = getOpportunityTotalsMap(dimension, year);
    string budgetingJSON;
    string budgetingItem;
    string monthlySubtotalsGeneral='';
    for ( Reports.GroupingValue groupingVal : dim.getGroupings() ) {
      string objectId = getObjectId('{'+(String)groupingVal.getValue()+'}');
      Map<Integer, Decimal> objectSnapshotMap = snapshotMap.get(objectId);
      String rowKey = (String)groupingVal.getKey();
      string monthlyTotalsJSON = getMonthlyTotals(groupingColumns, rowKey, factMap, objectSnapshotMap);
      //if( dimension == 'Profit_Center')
      monthlySubtotalsGeneral = getMonthlyTotals(groupingColumns, 'T', factMap, objectSnapshotMap);
      decimal rowTotal = getRowTotal(factMap, rowKey);
      string opportunityTotals = opportunityTotalsMap != null ? getOpportunityTotalsJSON(opportunityTotalsMap.get(objectId)) : '';
      string jsonItem = '{"Item" : {' + (String)groupingVal.getValue() + ',"subTotal":"' + rowTotal +'"'+opportunityTotals+'},"monthlyTotals" :' + monthlyTotalsJSON + '}';
      budgetingItem = budgetingItem != null && budgetingItem != '' ? budgetingItem + ',' + jsonItem : jsonItem;
    }
    string generalDataJSON = monthlySubtotalsGeneral != '' ? getGeneralData(year, factMap, monthlySubtotalsGeneral) + ',' : '';
    budgetingJSON = '{"General" : {' + generalDataJSON + '"Items":[' + budgetingItem + ']}}';
    return budgetingJSON;
  }

  private static Map<Id, Map<String, String>> getOpportunityTotalsMap(string dimension, string year){
    Map<Id, Map<String, String>> opportunityTotalsMap = new Map<Id, Map<String, String>>();
    // if( dimension == 'Profit_Center' )
    opportunityTotalsMap = getOpportunityTotals(year, dimension);
    return opportunityTotalsMap;
  }

  private static string getOpportunityTotalsJSON(Map<String,String> opportunityTotalsMap){
    string opportunityTotalsJSON='';
    if( opportunityTotalsMap != null ) {
      string jsonString = JSON.serialize(opportunityTotalsMap);
      opportunityTotalsJSON = ',' + jsonString.substringBetween('{','}');
    }
    return opportunityTotalsJSON;
  }

  public static Map<Id, Map<String, String>> getOpportunityTotals(string year, string dimension){
    Map<Id, Map<String, String>> opportunityTotalsMap = new Map<Id, Map<String, String>>();
    string reportName = 'BudgetingViewOpportunitiesTotals_'+dimension+ '_wgf';
    Reports.ReportFilter reportFilter = OrbisUtils.getReportFilter('Opportunity.DeliveryYear__c', 'equals', year);
    List<Reports.ReportFilter> repFilters = new List<Reports.ReportFilter>();
    repFilters.add(reportFilter);
    Reports.ReportResults results = OrbisUtils.runReport(reportName,false,repFilters);
    opportunityTotalsMap = OrbisUtils.getReportData(results);
    //system.debug('OpportunityTotals='+JSON.serialize(opportunityTotalsMap));
    return opportunityTotalsMap;
  }

  private static decimal getRowTotal(Map<String,Reports.ReportFact> factMap, string rowKey){
    decimal subtotal;
    List<Reports.SummaryValue> reportSubTotals = factMap.get(rowKey+'!T').getAggregates();
    subtotal = (Decimal)reportSubTotals.get(reportSubTotals.size()-1).getValue();
    subtotal = OrbisUtils.getFormattedDecimal(subtotal);
    return subtotal;
  }

  private static string getGeneralData(string year, Map<String,Reports.ReportFact> factMap, string monthlyTotals){
    string generalJSON;
    List<Reports.SummaryValue> reportTotals = factMap.get('T!T').getAggregates();
    Decimal grandTotal =  (Decimal)reportTotals[0].getValue();
    string snapshotGeneral = getLatestSnapshotGeneral();
    system.debug('snapshotGeneral='+snapshotGeneral);
    generalJSON = '"year":"' + year +'", "grandTotal":"' + grandTotal.setScale(0) + '",' + snapshotGeneral +'"monthlyGrandTotals":' + monthlyTotals;
    //generalJSON = '"year":"' + year +'", "grandTotal":"' + grandTotal.setScale(0) + '","monthlyGrandTotals":' + monthlyTotals;
    return generalJSON;
  }

  private static Map<Id, Map<Integer, Decimal>> getSnapshotMap(string dimension, string year){
    Map<Id, Map<Integer, Decimal>> snapshotMap = new Map<Id, Map<Integer, Decimal>>();
    string reportName = 'Budgeting_Snapshot_' + dimension + '_Report_wgf';
    string objectId;
    Reports.ReportFilter reportFilter = OrbisUtils.getReportFilter('Budgeting_Snapshot_'+dimension+'__c.Year__c', 'equals', year);
    List<Reports.ReportFilter> repFilters = new List<Reports.ReportFilter>();
    repFilters.add(reportFilter);
    Reports.ReportResults results = OrbisUtils.runReport(reportName,true,repFilters);
    Reports.Dimension dim = results.getGroupingsDown();
    system.debug('results='+results);
    List<Reports.GroupingValue> groupingValues = dim.getGroupings();
    for( Reports.GroupingValue groupingVal : groupingValues) {
      objectId = (String)groupingVal.getValue();
      String factMapKey = groupingVal.getKey() + '!T';
      Map<Integer, Decimal> monthlyAmounts = getSnapshotAmountMap(results, factMapKey);
      //system.debug('Snapshot objectId='+objectId.substring(0, 15));
      snapshotMap.put(objectId.substring(0, 15), monthlyAmounts);
    }
    //system.debug(snapshotMap);
    return snapshotMap;
  }

  private static Map<Integer, Decimal> getSnapshotAmountMap(Reports.ReportResults results, string factMapKey){
    Map<Integer, Decimal> monthlyAmountMap = new Map<Integer, Decimal>();
    Decimal amount;
    Reports.ReportCurrency amountData;
    Reports.ReportFactWithDetails factDetails =
      (Reports.ReportFactWithDetails)results.getFactMap().get(factMapKey);
    Reports.ReportDetailRow detailRow = factDetails.getRows()[0];
    Reports.ReportExtendedMetadata rmd = results.getReportExtendedMetadata();
    // Get detail columns from extended metadata
    Map<String,Reports.DetailColumn> colMap = rmd.getDetailColumnInfo();
    integer i = 0;
    for(String key : colMap.KeySet()) {
      //system.debug('get month called='+colMap.get(key).getLabel());
      i = getMonth(colMap.get(key).getLabel());
      if( i > 0 ) {
        amountData = (Reports.ReportCurrency)detailRow.getDataCells()[i].getValue();
        amount = amountData != null ? (decimal)amountData.getAmount() : 0;
        monthlyAmountMap.put(i,amount);
      }
    }
    return monthlyAmountMap;
  }

  public static integer getMonth(string strValue){
    integer month = 0;
    try{
      string monthValue = strValue.left(1) == 'M' ? strValue.substring(1) : null;
      month = integer.valueOf(monthValue);
    }
    catch(Exception e) {
      system.debug(e.getMessage());
    }
    return month;
  }

  private static string getObjectId(string rowKey){
    string objectId;
    Map<String,Object> objectData = (Map<String,Object>) JSON.deserializeUntyped(rowKey);
    objectId = objectData != null ? (String)objectData.values()[0] : '{}';
    //system.debug('Deserialize : '+objectData.values()[0]);
    return objectId;
  }

  private static Map <String, Object> getMonthlySubtotals(Map<String,Reports.ReportFact> factMap,
                                                          String rowKey,
                                                          List<Reports.GroupingValue> subColumns){
    Map <String, Object> monthlySubtotals = new Map <String, Object>();
    // List<MonthlySubtotals> monthlySubtotals = new List<MonthlySubtotals>();
    for( Reports.GroupingValue column  : subColumns) {
      string columnLabel = (String)column.getValue();
      string columnKey = (String)column.getKey();
      Decimal amount = OrbisUtils.getAmount(factMap, rowKey, columnKey);
      monthlySubtotals.put(columnLabel, OrbisUtils.getFormattedDecimal(amount));
    }
    return monthlySubtotals;
  }

  private static decimal getSnapshotAmount(integer month, Map<Integer,Decimal> snapshotMap){
    decimal snapshotAmount = snapshotMap != null && snapshotMap.get(month) != null ? snapshotMap.get(month) : 0;
    return snapshotAmount;
  }

  private static string getMonthlyTotals(List<Reports.GroupingValue> groupingColumns, String rowKey,
                                         Map<String,Reports.ReportFact> factMap,
                                         Map<Integer, Decimal> objectSnapshotMap){
    List<MonthlyData> monthlyDataList = new List<MonthlyData>();
    String monthlyTotalsJSON = '';
    integer nextMonth = 1;
    for( Reports.GroupingValue monthCol :  groupingColumns) {
      Date dateValue = (Date)monthCol.getValue();
      string columnKey = (String)monthCol.getKey();
      integer month = dateValue.month();
      List<MonthlyData> emptyMonthLst = getEmptyMonthsList(month, nextMonth, objectSnapshotMap);
      if( !emptyMonthLst.isEmpty() )
        monthlyDataList.addAll(emptyMonthLst);
      Decimal amount = OrbisUtils.getAmount(factMap, rowKey, columnKey);
      MonthlyData monthlyData = getMonthlyData(month, amount, objectSnapshotMap);
      monthlyData.subtotals = getMonthlySubtotals(factMap, rowKey, monthCol.getGroupings());
      //System.debug('monthlySubtotals=' +  monthlyData.subtotals);
      monthlyDataList.add(monthlyData);
      nextMonth = month + 1;
    }
    if(monthlyDataList.size() < 12) {
      monthlyDataList.addAll(addEmptyMonthsLatest(nextMonth, objectSnapshotMap));
    }
    monthlyTotalsJSON = JSON.serialize(monthlyDataList);
    //system.debug('monthlyTotalsJSON='+monthlyTotalsJSON);
    return monthlyTotalsJSON;
  }

  private static MonthlyData getMonthlyData(integer month, decimal amount, Map<Integer, Decimal> objectSnapshotMap){
    MonthlyData monthlyData = new MonthlyData();
    monthlyData.month = month;
    monthlyData.snapshotAmount = getSnapshotAmount(month, objectSnapshotMap);
    monthlyData.totalAmount = OrbisUtils.getFormattedDecimal(amount);
    return monthlyData;
  }

  private static List<MonthlyData> addEmptyMonthsLatest(integer nextMonth, Map<Integer, Decimal> objectSnapshotMap){
    List<MonthlyData> emptyMonthsLst = new List<MonthlyData>();
    for(integer k = nextMonth; k <= 12; k++) {
      system.debug('added month ='+k);
      MonthlyData monthData = getEmptyMonth(k, objectSnapshotMap);
      emptyMonthsLst.add(monthData);
    }
    return emptyMonthsLst;
  }

  private static List<MonthlyData> getEmptyMonthsList(integer month, integer nextMonth,
                                                      Map<Integer, Decimal> objectSnapshotMap){
    List<MonthlyData> emptyMonthsLst = new List<MonthlyData>();
    if( month > nextMonth ) {
      for(integer i = nextMonth; i<month; i++) {
        MonthlyData monthData = getEmptyMonth(i, objectSnapshotMap);
        emptyMonthsLst.add(monthData);
      }
    }
    return emptyMonthsLst;
  }

  private static MonthlyData getEmptyMonth(integer month, Map<Integer, Decimal> objectSnapshotMap){
    MonthlyData monthData = new MonthlyData();
    monthData.month = month;
    monthData.snapshotAmount = getSnapshotAmount(month, objectSnapshotMap);
    return monthData;
  }

  @AuraEnabled
  public static string saveBudgetingInputs(string inputsJSON, string objectType){
    string result = 'No snapshot values to insert ';
    try{
      string sObjType = 'Budgeting_Snapshot_' + objectType + '__c';
      Schema.SObjectType sObjectType = Schema.getGlobalDescribe().get(sObjType);
      String listType = 'List<' + sObjectType + '>';
      //system.debug('InputsJSON = ' + inputsJSON);
      Type t = Type.forName(listType);
      List<sObject> inputs =  (List<sObject>) JSON.deserialize(inputsJSON,t);
      system.debug('Object = ' + inputs);
      if( inputs != null) {
        insert inputs;
        result = inputs.size() + ' ' + objectType + ' values were inserted successfully.';
      }
    }catch(Exception e) {
      result = e.getMessage();
      system.debug('Error in parcing Budgeting snapshot JSON message : '+e.getMessage());
    }
    system.debug('Result : '+result);
    return result;
  }

  private static string getLatestSnapshotGeneral(){
    string jsonSnapshot='';
    List <Budgeting_Snapshot_Profit_Center__c> latestSnapshotLst = [SELECT Id, createdBy.Name, createdDate
                                                                    FROM Budgeting_Snapshot_Profit_Center__c
                                                                    ORDER BY createdDate desc LIMIT 1];
    if( latestSnapshotLst.size() > 0 ) {
      Datetime dt = latestSnapshotLst[0].createdDate;
      Date newDate = date.newinstance(dt.year(), dt.month(), dt.day());
      jsonSnapshot = ' "snapshotCreatedDate" : "' + dt.format('MMMMM dd, yyyy') +'","snapshotCreatedBy" : "' + latestSnapshotLst[0].createdBy.Name+'",';
    }
    return jsonSnapshot;
  }

  private class MonthlyData {
    private integer month;
    private decimal totalAmount;
    private decimal snapshotAmount;
    Map <String, Object> subtotals;
  }

  private class MonthlySubtotals {
    private decimal amount;
    private string opportunityType;
  }
}