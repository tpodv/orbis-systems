global class OrbisUtils {

  static void SendEmail(List<Messaging.SingleEmailMessage> emails){
    Messaging.sendEmail(emails);
  }

  public static Messaging.SingleEmailMessage getEmail(string emailBody, string subject, List<String> emailTo){
    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
    email.setToAddresses(emailTo);
    email.setSubject(subject);
    email.setHtmlBody(emailBody);
    return email;
  }

  public static String getAsciiCode(integer code){
    List<Integer> charArr = new Integer[] {code};
    String asciiChar = String.fromCharArray(charArr);
    return asciiChar;
  }

  public static String getClearlearString(string str){
    if( str != null && str !='') {
      str = str.trim();
      str = str.replaceAll('(\\s+)', '');
      string spaceChar = getAsciiCode(160);
      str = str.replaceAll(spaceChar, '');
    }
    return str;
  }

  public static Reports.ReportResults runReport(string reportName, boolean hasDetails, List<Reports.ReportFilter> repFilters){
    string reportId;
    List <Report> reportList = [SELECT Id, DeveloperName FROM Report WHERE
                                DeveloperName = :reportName];
    if( !reportList.isEmpty() )
      reportId = (String)reportList.get(0).get('Id');
    else return null;
    // add filter to report
    Reports.ReportDescribeResult describe = Reports.ReportManager.describeReport(reportId);
    Reports.ReportMetadata reportMd = describe.getReportMetadata();
    if( repFilters != null ) {
      reportMd.getReportFilters().addAll(repFilters);
      system.debug('Filter for '+reportName+'='+reportMd.getReportFilters());
    }
    // Run the report
    Reports.ReportResults results = Reports.ReportManager.runReport(reportId, reportMd, hasDetails);
    System.debug('Synchronous results : ' + results);
    return results;
  }

  public static Reports.ReportFilter getReportFilter(string columnName, string operator, string value){
    Reports.ReportFilter reportFilter = new Reports.ReportFilter();
    reportFilter.setColumn(columnName);
    reportFilter.setOperator(operator);
    reportFilter.setValue(value);
    return reportFilter;
  }

  public static Map<Id, Map<String, String>> getReportData(Reports.ReportResults results){
    Map<Id, Map<String, String>> reportDataMap = new Map<Id, Map<String, String>>();
    system.debug('reporting result='+results);
    if( results == null )
      return null;
    Reports.Dimension dim = results.getGroupingsDown();
    for ( Reports.GroupingValue groupingVal : dim.getGroupings() ) {
      string objectId = (String)groupingVal.getValue();
      string rowKey = (String)groupingVal.getKey();
      Map<String,String> reportDataValueMap = getReportDataValues(results, rowKey);
      reportDataMap.put(objectId, reportDataValueMap);
    }
    return reportDataMap;
  }

  public static Map<String,String> getReportDataValues(Reports.ReportResults results, string rowKey){
    Map<String,String> reportDataValueMap = new Map<String,String>();
    List<Reports.GroupingValue> groupingColumns = (List<Reports.GroupingValue>)results.getGroupingsAcross().getGroupings();
    Map<String,Reports.ReportFact> factMap = (Map<String,Reports.ReportFact>)results.getFactMap();
    for( Reports.GroupingValue col :  groupingColumns) {
      String columnLabel = (String)col.getValue();
      String columnKey = (String)col.getKey();
      Decimal amount = (Decimal)getAmount(factMap, rowKey, columnKey);
      amount = OrbisUtils.getFormattedDecimal(amount);
      reportDataValueMap.put(columnLabel, string.valueOf(amount));
    }
    return reportDataValueMap;
  }

  public static decimal getAmount(Map<String,Reports.ReportFact> factMap, string rowKey, string columnKey){
    Reports.ReportFactWithSummaries factSum = (Reports.ReportFactWithSummaries)factMap.get(rowKey + '!' + columnKey);
    Reports.SummaryValue sumVal = factSum.getAggregates()[0];
    Decimal amount = (Decimal)sumVal.getValue();
    //system.debug('Amount=' + amount);
    return amount;
  }

  public static decimal getFormattedDecimal(decimal amount){
    amount = amount != 0 ? amount/1000 : 0;
    amount = amount < 1 && amount != 0 ? amount.setScale(1) : amount.setScale(0);
    return amount;
  }
}