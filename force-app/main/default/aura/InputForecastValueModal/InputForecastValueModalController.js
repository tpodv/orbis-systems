({
  handleCancel: function (cmp, event, helper) {
    cmp.set('v.isOpen', false);
  },

  openModal: function (cmp, event, helper) {
    //we are fetching parameters from event that calls this method.
    /* var params = event.getParam('arguments') || event.getParams();
     cmp.set('v.isOpen', params);*/
    var params = event.getParam('arguments');
    if (params) {
      cmp.set("v.changedForecastValue", params[1]);
      cmp.set('v.isOpen', params);
    }
  },

  closeModal: function (cmp, event, helper) {
    cmp.set("v.inputValue", parseInt(cmp.get("v.changedForecastValue"), 10));
    //cmp.set("v.inputValue", cmp.get("v.changedForecastValue"));
    cmp.set('v.isOpen', false);
    console.log('Input=' + cmp.get("v.inputValue"));
    var cmpTarget = cmp.find('Modalbox');
  }
});