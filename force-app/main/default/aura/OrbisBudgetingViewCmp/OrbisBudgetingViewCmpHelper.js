({
  uploadBudgetingData: function (cmp, event, helper, dim) {
    var result = [];
    var action = cmp.get("c.getBudgetingTotals");
    var budgetingYear = cmp.get("v.displayYear");
    action.setParams({
      dimension: dim,
      year: budgetingYear
    });
    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var resultJSON = response.getReturnValue();
        var parsed = JSON.parse(resultJSON);
        result = parsed.General;
        console.log("Result:" + JSON.stringify(result));
        cmp.set("v." + dim + "Data", result);
      } else if (state === "INCOMPLETE") {} else if (state === "ERROR") {
        var errors = response.getError();
        if (errors) {
          if (errors[0] && errors[0].message) {
            console.log("Error message: " + errors[0].message);
            cmp.set("v.resultMessage", "Error message:  " + errors[0].message);
          }
        } else {
          console.log("Unknown error");
        }
      }
    });
    $A.enqueueAction(action);
    return result;
  },

  uploadData: function (cmp, event, helper) {
    helper.uploadBudgetingData(cmp, event, helper, "Profit_Center");
    helper.uploadBudgetingData(cmp, event, helper, "Account");
  },

  getSnapshotJSON: function (cmp, event, helper, snapshotMap) {
    var snapshotJSON = {};
    var snapshotData = "";

    for (let key of snapshotMap.keys()) {
      var record = "";
      var snapshotRecordList = snapshotMap.get(key);
      for (var i = 0; i < snapshotRecordList.length; i++) {
        var item =
          '"' +
          snapshotRecordList[i].key +
          '":"' +
          snapshotRecordList[i].value +
          '"';
        record = record == "" ? item : record + "," + item;
      }
      record = "{" + record + "}";
      snapshotData = snapshotData == "" ? record : snapshotData + "," + record;
    }
    var snapshotData = "[" + snapshotData + "]";
    //snapshotJSON = JSON.stringify(snapshotData);
    console.log(snapshotData);
    return snapshotData;
  },

  getSnapshotMap: function (cmp, event, helper, inputs, snapshotMap) {
    for (var i = 0; i < inputs.length; i++) {
      var value = inputs[i].get("v.value");
      if (value != undefined && value != null) {
        //var amountData = name.split("_");
        var id = inputs[i].get("v.object-id");
        var salesAmount = inputs[i].get("v.sales-amount");
        var forecastAmount = inputs[i].get("v.forecast-amount");
        var actualSalesAmount = inputs[i].get("v.actual-sales-amount");
        var month = inputs[i].get("v.month");
        var originalValue = inputs[i].get("v.total");
        var snapshotRecord = snapshotMap.get(id);
        var objType = inputs[i].get("v.object-type");
        if (!snapshotRecord) {
          snapshotRecord = [];
          if (objType == "account") {
            snapshotRecord.push({
              key: "Account__c",
              value: id
            });
          } else {
            snapshotRecord.push({
              key: "Profit_centre__c",
              value: id
            });
          }
        }
        snapshotRecord.push({
          key: "Year__c",
          value: cmp.get("v.displayYear")
        });
        snapshotRecord.push({
          key: "M" + month + "__c",
          value: value
        });
        snapshotRecord.push({
          key: "TotalM" + month + "__c",
          value: originalValue
        });
        snapshotRecord.push({
          key: "SalesM" + month + "__c",
          value: salesAmount != undefined ? salesAmount : 0
        });
        snapshotRecord.push({
          key: "ForecastM" + month + "__c",
          value: forecastAmount != undefined ? forecastAmount : 0
        });
        snapshotRecord.push({
          key: "ActualSalesM" + month + "__c",
          value: actualSalesAmount != undefined ? actualSalesAmount : 0
        });
        snapshotMap.set(id, snapshotRecord);
      }
    }
    return snapshotMap;
  },

  getAccountData: function (cmp, event, helper) {
    var snapshotMap = new Map();
    var accountCmp = cmp.find("accountCmp");
    var accountInputs = [];
    if (accountCmp) {
      for (var i = 0; i < accountCmp.length; i++) {
        var inputs = accountCmp[i].find("amount");
        if (inputs) {
          var concatInputs = [];
          concatInputs = accountInputs.concat(inputs);
          accountInputs = concatInputs;
        }
      }
      console.log("Account amount:" + accountInputs.length);
    }
    return accountInputs;
  },

  saveBudgetingInputsPromise: function (
    cmp,
    event,
    helper,
    inputsJSONstring,
    objType
  ) {
    return new Promise(function (resolve, reject) {
      var resultMessage = "";
      var action = cmp.get("c.saveBudgetingInputs");
      action.setParams({
        inputsJSON: inputsJSONstring,
        objectType: objType
      });
      helper
        .serverSideCall(cmp, action)
        .then(
          $A.getCallback(function (response) {
            resultMessage = response;
            cmp.set("v.saveResult", resultMessage);
            resolve(resultMessage);
          })
        )
        .catch(
          $A.getCallback(function (error) {
            component.set("v.saveResult", error);
            reject();
          })
        );
    });
  },

  serverSideCall: function (cmp, action) {
    return new Promise(function (resolve, reject) {
      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          resolve(response.getReturnValue());
        } else {
          reject(new Error(response.getError()));
        }
      });
      $A.enqueueAction(action);
    });
  },

  saveBudgetingInputs: function (cmp, event, inputsJSONstring, objType) {
    var action = cmp.get("c.saveBudgetingInputs");
    var resultMessage = "";
    action.setParams({
      inputsJSON: inputsJSONstring,
      objectType: objType
    });
    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        resultMessage = response.getReturnValue();
        cmp.set("v.saveResult", resultMessage);
      } else if (state === "INCOMPLETE") {} else if (state === "ERROR") {
        var errors = response.getError();
        if (errors) {
          if (errors[0] && errors[0].message) {
            console.log("Error message: " + errors[0].message);
            resultMessage = "Error message:  " + errors[0].message;
            cmp.set("v.saveResult", resultMessage);
          }
        } else {
          console.log("Unknown error");
        }
      }
    });
    $A.enqueueAction(action);
  },

  saveObjectDataPromise: function (cmp, event, helper, inputData, objType) {
    return new Promise(function (resolve, reject) {
      var resultMessage = "";
      var snapshotMap = new Map();
      snapshotMap = helper.getSnapshotMap(
        cmp,
        event,
        helper,
        inputData,
        snapshotMap
      );
      var snapshotJSON = helper.getSnapshotJSON(
        cmp,
        event,
        helper,
        snapshotMap
      );
      console.log("snapshotJSON: " + snapshotJSON);
      helper
        .saveBudgetingInputsPromise(cmp, event, helper, snapshotJSON, objType)
        .then(
          function (result) {
            resultMessage = cmp.get("v.saveResult");
            console.log("Promise was resolved: ");
            resolve("Resolved");
          },
          // reject handler
          function (error) {
            console.log("Promise was rejected: ", error);
            reject(error);
          }
        );
    });
    console.log("resultMessage=" + resultMessage);
  },

  saveObjectData: function (cmp, event, helper, inputData, objType) {
    var resultMessage = "";
    var snapshotMap = new Map();
    snapshotMap = helper.getSnapshotMap(
      cmp,
      event,
      helper,
      inputData,
      snapshotMap
    );
    var snapshotJSON = helper.getSnapshotJSON(cmp, event, helper, snapshotMap);
    helper.saveBudgetingInputsPromise(
      cmp,
      event,
      helper,
      snapshotJSON,
      objType
    );
    resultMessage = cmp.get("v.saveResult");
    console.log("resultMessage=" + resultMessage);
    return resultMessage;
  },

  showSaveModal: function (cmp, message) {
    var resultMessage = cmp.get("v.saveResult");
    $A.createComponent("c:OverlayLibraryModal", {}, function (content, status) {
      if (status === "SUCCESS") {
        var modalBody = content;
        cmp
          .find("overlayLib")
          .showCustomModal({
            header: message,
            body: content,
            showCloseButton: true,
            closeCallback: function (ovl) {
              console.log("Overlay is closing");
            }
          })
          .then(function (overlay) {
            console.log("Overlay is made");
          });
      }
    });
  }
});