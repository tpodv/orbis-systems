({
  doInit: function (cmp, event, helper) {
    document.title = "Budgeting View";
    var today = new Date();
    cmp.set("v.displayYear", today.getFullYear());
    cmp.set("v.selectYear", today.getFullYear() + 1);
    helper.uploadData(cmp, event, helper);
  },

  toggleYear: function (cmp, event, helper) {
    var displayYear = cmp.get("v.displayYear");
    var selectYear = cmp.get("v.selectYear");
    cmp.set("v.displayYear", selectYear);
    cmp.set("v.selectYear", displayYear);
    helper.uploadData(cmp, event, helper);
  },

  onInputValueChange: function (cmp, event, helper) {
    var newValue = cmp.get("v.inputForecastValue");
    var items = cmp.get("v.Profit_CenterData.Items");
    var curInputMonth = cmp.get("v.currentInputMonth");
    var curInputId = cmp.get("v.currentInputId");
    for (var i in items) {
      var id = items[i].Item.profitCenterId;
      if (id == curInputId) {
        var monthlyTotals = items[i].monthlyTotals;
        for (var m in monthlyTotals) {
          var month = monthlyTotals[m].month;
          if (month == curInputMonth) {
            monthlyTotals[m].snapshotAmount = newValue != null && newValue != '' ? newValue : 0;
            cmp.set("v.Profit_CenterData.Items", items);
          }
        }
      }
    }
  },

  openModal: function (cmp, event, helper) {
    // We are finding the child component on markup and executing its aura method and passing the parameters to it.
    cmp.find("showModal").showChildModal(true);
  },

  openInputModal: function (cmp, event, helper) {
    // We are finding the child component on markup and executing its aura method and passing the parameters to it.
    cmp.set("v.currentInputId", event.getSource().get("v.object-id"));
    cmp.set("v.currentInputMonth", event.getSource().get("v.month"));
    cmp.find("showInputModal").showChildModal(true, event.getSource().get("v.value"));
  },

  showSpinner: function (cmp, event, helper) {
    // make Spinner attribute true for display loading spinner
    cmp.set("v.Spinner", true);
  },

  // this function automatic call by aura:doneWaiting event
  hideSpinner: function (cmp, event, helper) {
    // make Spinner attribute to false for hide loading spinner
    cmp.set("v.Spinner", false);
  },

  toggleProfitCenter: function (cmp, event, helper) {
    var target = event.target;
    var profitCenterId = target.getAttribute("data-pcid");
    var selectedRows = cmp.get("v.selectedRows");
    var item = selectedRows.indexOf(profitCenterId);
    if (item > -1) {
      selectedRows.splice(item, 1);
    } else {
      selectedRows.push(profitCenterId);
    }
    cmp.set("v.selectedRows", selectedRows);
  },

  saveSnapshot: function (cmp, event, helper) {
    var inputs = cmp.find("amount");
    var accountInputs = helper.getAccountData(cmp, event, helper);
    Promise.all([
      /*helper.saveObjectDataPromise(
        cmp,
        event,
        helper,
        accountInputs,
        "Account"
      ),*/
      helper.saveObjectDataPromise(cmp, event, helper, inputs, "Profit_Center")
    ]).then(
      function (result) {
        helper.showSaveModal(cmp, "Snapshots are saved successfully. Result:" + result);
      },
      // reject handler
      function (error) {
        console.log("Promise was rejected: ", error);
        helper.showSaveModal(cmp, "Error saving snapshots: " + error);
      }
    );
  },

  handleSaveEvent: function (cmp, event) {
    var message = event.getParam("message");
    if (message == "Close") {
      console.log("Close event is fired");
      // if the user clicked the OK button do your further Action here
    }
  }
});