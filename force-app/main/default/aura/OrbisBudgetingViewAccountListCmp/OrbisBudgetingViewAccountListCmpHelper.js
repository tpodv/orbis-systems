({
  uploadBudgetingData: function(cmp, event, helper, dim) {
    var result = [];
    var action = cmp.get("c.getBudgetingTotals");
    action.setParams({ dimension: dim });
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var resultJSON = response.getReturnValue();
        var parsed = JSON.parse(resultJSON);
        result = parsed.General;
        cmp.set("v." + dim + "Data", result);
      } else if (state === "INCOMPLETE") {
      } else if (state === "ERROR") {
        var errors = response.getError();
        if (errors) {
          if (errors[0] && errors[0].message) {
            console.log("Error message: " + errors[0].message);
            cmp.set("v.resultMessage", "Error message:  " + errors[0].message);
          }
        } else {
          console.log("Unknown error");
        }
      }
    });
    $A.enqueueAction(action);
    return result;
  },
});