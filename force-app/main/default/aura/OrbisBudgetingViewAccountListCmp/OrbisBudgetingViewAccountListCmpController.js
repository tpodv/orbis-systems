({
  navigateToRecord: function(cmp, event, helper) {
    var recordId = event.target.getAttribute('data-index');
    window.open('/' + recordId, '_blank');
  }
});