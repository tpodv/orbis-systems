({
  handleClose: function(cmp, event, helper) {
    //closes the modal or popover from the component
    var appEvent = $A.get("e.c:OverlayLibraryModalEvent");
    appEvent.setParams({
      message: "Close"
    });
    appEvent.fire();
    cmp.find("overlayLib").notifyClose();
  }
});
