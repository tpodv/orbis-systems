({
  uploadHelper: function(cmp, event) {
    var fileInput = cmp.find("fuploader").get("v.files");
    var self = this;
    // get the first file using array index[0]
    var file = fileInput[0];
    var objFileReader = new FileReader();
    objFileReader.onload = $A.getCallback(function() {
      var fileContents = objFileReader.result;
      console.log("File content", fileContents);
      var base64 = "base64,";
      var dataStart = fileContents.indexOf(base64) + base64.length;
      fileContents = fileContents.substring(dataStart);
      self.processHelper(cmp, event, fileContents);
    });
    objFileReader.readAsText(file);
  },

  processHelper: function(cmp, event, fileContent) {
    var action = cmp.get("c.createKPIOpportunities");
    action.setParams({
      fileContent: encodeURIComponent(fileContent),
    });
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        console.log("Result:" + response.getReturnValue());
        cmp.set("v.resultMessage", response.getReturnValue());
        //var result = $A.get("e.force:closeQuickAction").fire();
      } else if (state === "INCOMPLETE") {
      } else if (state === "ERROR") {
        var errors = response.getError();
        if (errors) {
          if (errors[0] && errors[0].message) {
            console.log("Error message: " + errors[0].message);
            cmp.set("v.resultMessage", "Error message:  " + errors[0].message);
          }
        } else {
          console.log("Unknown error");
        }
      }
    });
    $A.enqueueAction(action);
  },
});