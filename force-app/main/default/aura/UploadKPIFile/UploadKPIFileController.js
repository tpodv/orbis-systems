({
  handleUploadFinished: function (cmp, event, helper) {
    var fileName = 'No File Selected..';
    if (event.getSource().get('v.files').length > 0) {
      fileName = event.getSource().get('v.files')[0]['name'];
    }
    cmp.set('v.fileName', fileName);
  },

  handleSave: function (cmp, event, helper) {
    if (cmp.find('fuploader').get('v.files').length > 0) {
      helper.uploadHelper(cmp, event);
    } else {
      alert('Please Select a Valid File');
    }
  },

  handleCancel: function (cmp, event, helper) {
    $A.get('e.force:closeQuickAction').fire();
  },

  showSpinner: function (cmp, event, helper) {
    // make Spinner attribute true for display loading spinner
    cmp.set('v.Spinner', true);
  },

  // this function automatic call by aura:doneWaiting event
  hideSpinner: function (cmp, event, helper) {
    // make Spinner attribute to false for hide loading spinner
    cmp.set('v.Spinner', false);
  },

  openModal: function (cmp, event, helper) {
    //we are fetching parameters from event that calls this method.
    var params = event.getParam('arguments') || event.getParams();
    cmp.set('v.isOpen', params);
  },

  closeModal: function (cmp, event, helper) {
    cmp.set('v.isOpen', false);
    cmp.set('v.resultMessage', '');
  }
});